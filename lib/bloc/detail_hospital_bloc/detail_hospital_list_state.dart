part of 'detail_hospital_list_bloc.dart';

abstract class DetailHospitalListState extends Equatable {
  const DetailHospitalListState();
}

class LoadingState extends DetailHospitalListState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ErrorState extends DetailHospitalListState {
  final String message;
  @override
  // TODO: implement props
  List<Object> get props => [message];

  ErrorState(this.message);
}

class ShowSnackBar extends DetailHospitalListState {
  final String message;
  ShowSnackBar(this.message);
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class LoadedStateDetail extends DetailHospitalListState {
  final List<List<dynamic>> hospitalCompareData;

  LoadedStateDetail(this.hospitalCompareData);

  @override
  List<Object> get props => [hospitalCompareData];
}
